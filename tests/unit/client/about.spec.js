import {
    shallowMount
} from "@vue/test-utils";
import About from "@/views/About.vue";

describe("About.vue", () => {
    it("render props.description", () => {
        const description = "Learning to make an amazing Vue Enterprise App";
        const wrapper = shallowMount(About, {
            propsData: {
                description
            }
        })
        expect(wrapper.text()).toMatch(description)
    })
})